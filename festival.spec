Name:           festival
Version:        1.96
Release:        46
Summary:        Festival Speech Synthesis System
License:        MIT and GPL+ and TCL
URL:            http://www.cstr.ed.ac.uk/projects/festival/

%define docversion 1.4.2
%define speechtoolsversion 1.2.96
%define baseURL http://festvox.org/packed/festival/%{version}
%define diphoneversion 0.19990610
%define nitechbaseURL http://hts.sp.nitech.ac.jp/?plugin=attach&refer=Download&openfile=
%define nitechhtsversion 0.20061229
%define hispavocesversion 1.0.0

Source0:        %{baseURL}/festival-%{version}-beta.tar.gz
Source1:        %{baseURL}/speech_tools-%{speechtoolsversion}-beta.tar.gz
Source2:        http://festvox.org/packed/festival/%{docversion}/festdoc-%{docversion}.tar.gz
Source100:      %{baseURL}/festlex_POSLEX.tar.gz
Source101:      %{baseURL}/festlex_CMU.tar.gz
Source200:      %{baseURL}/festvox_kallpc16k.tar.gz
Source202:      %{baseURL}/festvox_kedlpc16k.tar.gz
Source220:      %{nitechbaseURL}/festvox_nitech_us_awb_arctic_hts.tar.bz2
Source221:      %{nitechbaseURL}/festvox_nitech_us_bdl_arctic_hts.tar.bz2
Source222:      %{nitechbaseURL}/festvox_nitech_us_clb_arctic_hts.tar.bz2
Source223:      %{nitechbaseURL}/festvox_nitech_us_jmk_arctic_hts.tar.bz2
Source224:      %{nitechbaseURL}/festvox_nitech_us_rms_arctic_hts.tar.bz2
Source225:      %{nitechbaseURL}/festvox_nitech_us_slt_arctic_hts.tar.bz2
Source300:      http://v4.guadalinex.org/guadalinex-toro/pool-test/main/f/festival-spanish-voices/festival-spanish-voices_1.0.0.orig.tar.gz
Source301:      COPYING.hispavoces

Patch1:         festival-1.96-nitech-american.patch
Patch2:         festival_buildroot.patch
Patch3:         festival-1.96-speechtools-shared-build.patch
Patch5:         festival-1.96-speechtools-rateconvtrivialbug.patch
Patch6:         festival-1.96-speechtools-linklibswithotherlibs.patch
Patch7:         festival-1.96-speechtools-ohjeezcxxisnotgcc.patch
Patch8:         festival-1.96-etcsiteinit.patch
Patch9:         festival-1.96-alias_cmu_to_nitech.patch
Patch10:        festival-1.96-findspeechtools.patch
Patch11:        festival-1.96-main-shared-build.patch
Patch12:        festival-1.96-bettersonamehack.patch
Patch20:        festival-1.96-speechtools-1.2.96-beta+awb.patch
Patch31:        festival-1.96-kludge-etcpath-into-libarch.patch
Patch60:        festival-1.96-format-security.patch
Patch90:        festival-1.96-nitech-proclaimvoice.patch
Patch91:        festival-1.96-nitech-fixmissingrequire.patch
Patch92:        festival-1.96-nitech-sltreferences.patch
Patch93:        gcc43.patch
Patch94:        festival-speech-tools-pulse.patch
Patch95:        gcc44.patch
Patch96:        festival.gcc47.patch
Patch97:        no-shared-data.patch
Patch98:        festival-1.96-server-script-typo.patch
Patch99:        festival-gcc7.patch
Patch100:       Fix-festival-gcc10.patch
#https://build.opensuse.org/package/show/openSUSE:Factory/festival
Patch101:       CVE-2010-3996-festival-no-LD_LIBRARY_PATH-extension.patch
Patch102:       CVE-2010-3996-festival-safe-temp-file.patch
Patch103:       CVE-2010-3996-speech_tools-no-LD_LIBRARY_PATH-extension.patch
Patch104:       fix-add-the-compilation-option-pie.patch

BuildRequires:  gcc gcc-c++ pulseaudio-libs-devel texi2html ncurses-devel
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

Provides:       %{name}-lib = %{version}-%{release}
Provides:       %{name}-speechtools-libs = %{speechtoolsversion}-%{release}
Provides:       %{name}-speechtools-utils = %{speechtoolsversion}-%{release}
Provides:       festvox-kal-diphone = %{diphoneversion}-%{release}
Provides:       festvox-ked-diphone = %{diphoneversion}-%{release}
Provides:       festvox-awb-arctic-hts = %{nitechhtsversion}-%{release}
Provides:       festvox-bdl-arctic-hts = %{nitechhtsversion}-%{release}
Provides:       festvox-clb-arctic-hts = %{nitechhtsversion}-%{release}
Provides:       festvox-jmk-arctic-hts = %{nitechhtsversion}-%{release}
Provides:       festvox-rms-arctic-hts = %{nitechhtsversion}-%{release}
Provides:       festvox-slt-arctic-hts = %{nitechhtsversion}-%{release}
Provides:       hispavoces-pal-diphone = %{hispavocesversion}-%{release}
Provides:       hispavoces-sfl-diphone = %{hispavocesversion}-%{release}
Provides:       festival-voice festvox-kallpc16k festvox-kedlpc16k
Obsoletes:      %{name}-lib < %{version}-%{release}
Obsoletes:      %{name}-speechtools-libs < %{speechtoolsversion}-%{release}
Obsoletes:      %{name}-speechtools-utils < %{speechtoolsversion}-%{release}
Obsoletes:      festvox-kal-diphone < %{diphoneversion}-%{release}
Obsoletes:      festvox-ked-diphone < %{diphoneversion}-%{release}
Obsoletes:      festvox-awb-arctic-hts < %{nitechhtsversion}-%{release}
Obsoletes:      festvox-bdl-arctic-hts < %{nitechhtsversion}-%{release}
Obsoletes:      festvox-clb-arctic-hts < %{nitechhtsversion}-%{release}
Obsoletes:      festvox-jmk-arctic-hts < %{nitechhtsversion}-%{release}
Obsoletes:      festvox-rms-arctic-hts < %{nitechhtsversion}-%{release}
Obsoletes:      festvox-slt-arctic-hts < %{nitechhtsversion}-%{release}
Obsoletes:      hispavoces-pal-diphone < %{hispavocesversion}-%{release}
Obsoletes:      hispavoces-sfl-diphone < %{hispavocesversion}-%{release}

%description
Festival offers a general framework for building speech synthesis systems
as well as including examples of various modules. As a whole it offers full
text to speech through a number APIs: from shell level, though a Scheme
command interpreter, as a C++ library, from Java, and an Emacs interface.

%package devel
Summary:        Header files for festival
Version:        %{version}
Requires:       %{name} = %{version}-%{release}

Provides:       %{name}-speechtools-devel = %{speechtoolsversion}-%{release}
Obsoletes:      %{name}-speechtools-devel < %{speechtoolsversion}-%{release}

%description devel
Header files for festival.

%package help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info
Requires(post): /sbin/install-info
Requires(postun): /sbin/install-info

Provides:       %{name}-docs = %{docversion}-%{release}
Obsoletes:      %{name}-docs < %{docversion}-%{release}

%description help
Man pages and other related documents for %{name}.

%prep
%setup -q -n festival -a 1
%setup -q -n festival -D -T -a 2

[ -x speech_tools/base_class/string/EST_strcasecmp.c ] || exit 1
chmod -x speech_tools/base_class/string/EST_strcasecmp.c

%setup -q -n festival -D -T -b 100
%setup -q -n festival -D -T -b 101
%setup -q -n festival -D -T -b 200
%setup -q -n festival -D -T -b 202
%setup -q -n festival -D -T -b 220
%setup -q -n festival -D -T -b 221
%setup -q -n festival -D -T -b 222
%setup -q -n festival -D -T -b 223
%setup -q -n festival -D -T -b 224
%setup -q -n festival -D -T -b 225
%setup -c -q -n festival -D -T -a 300

%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
rm -f lib/alias_cmu_to_nitech.scm.cmu2nitech
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch20 -p1
%patch31 -p1
for f in speech_tools/main/siod_main.cc src/arch/festival/festival.cc; do
  sed -i -e 's,{{HORRIBLELIBARCHKLUDGE}},"%{_libdir}",' $f
done

%patch60 -p1
%patch90 -p1
%patch91 -p1
%patch92 -p1
%patch93 -p1
%patch94 -p1
%patch95 -p1
%patch96 -p0
%patch97 -p1
%patch98
%patch99 -p1
%patch100 -p1
%patch101 -p1
%patch102 -p1
%patch103 -p1
%patch104 -p1

rm festdoc-%{docversion}/speech_tools/doc/index_html.jade
rm festdoc-%{docversion}/speech_tools/doc/tex_stuff.jade
rm festdoc-%{docversion}/speech_tools/doc/examples_gen/error_example_section.sgml

%build
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/speech_tools/lib
pushd speech_tools
  %configure
  make \
    CFLAGS="$RPM_OPT_FLAGS -fPIC -fno-strict-aliasing -fpermissive" \
    CXXFLAGS="$RPM_OPT_FLAGS  -fPIC -fno-strict-aliasing -fpermissive"
popd

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/src/lib
export PATH=$(pwd)/bin:$PATH
%configure
make \
  FTLIBDIR="%{_datadir}/festival/lib" \
  CFLAGS="$RPM_OPT_FLAGS -fPIC -fpermissive" \
  CXXFLAGS="$RPM_OPT_FLAGS -fPIC -fpermissive"

pushd lib/dicts/cmu
  make
popd

%install
pushd speech_tools
  make INSTALLED_LIB=%{buildroot}%{_libdir} make_installed_lib_shared
  rm %{buildroot}%{_libdir}/*.a
  make INSTALLED_BIN=%{buildroot}%{_libexecdir}/speech-tools make_installed_bin_static

  pushd %{buildroot}%{_libexecdir}/speech-tools
    ls |
        grep -Evw "ch_wave|ch_track|na_play|na_record|wagon|wagon_test" |
        grep -Evw "make_wagon_desc|pitchmark|pm|sig2fv|wfst_build" |
        grep -Evw "wfst_run|wfst_run" |
        xargs rm
  popd

  pushd include
    for d in $( find . -type d | grep -v win32 ); do
      make -w -C $d INCDIR=%{buildroot}%{_includedir}/speech_tools/$d install_incs
    done
    mv %{buildroot}%{_includedir}/speech_tools/EST/*.h \
       %{buildroot}%{_includedir}/speech_tools/
    rmdir %{buildroot}%{_includedir}/speech_tools/EST
    mv %{buildroot}%{_includedir}/speech_tools/unix/EST/EST_* \
       %{buildroot}%{_includedir}/speech_tools/unix/
    rmdir %{buildroot}%{_includedir}/speech_tools/unix/EST
    mv %{buildroot}%{_includedir}/speech_tools/instantiate/EST/instantiate/EST_* \
       %{buildroot}%{_includedir}/speech_tools/instantiate/
    rm -rf %{buildroot}%{_includedir}/speech_tools/instantiate/EST
    mv %{buildroot}%{_includedir}/speech_tools/sigpr/EST/EST_* \
       %{buildroot}%{_includedir}/speech_tools/sigpr
    rmdir %{buildroot}%{_includedir}/speech_tools/sigpr/EST
    mv %{buildroot}%{_includedir}/speech_tools/ling_class/EST/EST_* \
       %{buildroot}%{_includedir}/speech_tools/ling_class
    rmdir %{buildroot}%{_includedir}/speech_tools/ling_class/EST
  popd

  cp README ../README.speechtools
popd

TOPDIR=$( pwd )
pushd lib/dicts
  mkdir -p %{buildroot}%{_datadir}/festival/lib/dicts
  cp COPYING.poslex $OLDPWD/COPYING.poslex
  cp cmu/COPYING $OLDPWD/COPYING.cmudict
  for f in wsj.wp39.poslexR wsj.wp39.tri.ngrambin ; do
    install -m 644 $f %{buildroot}%{_datadir}/festival/lib/dicts/
  done
  mkdir -p %{buildroot}%{_datadir}/festival/lib/dicts/cmu

  pushd cmu
    for f in allowables.scm cmudict-0.4.diff cmudict-0.4.out \
             cmudict_extensions.scm cmulex.scm cmu_lts_rules.scm; do
      install -m 644 $f %{buildroot}%{_datadir}/festival/lib/dicts/cmu/
    done
  popd
popd

pushd lib/voices
  for f in $( find . -name COPYING ); do
    n=$( echo $f | sed 's/.*\/\(.*\)\/COPYING/COPYING.\1/' )
    mv $f $OLDPWD/$n
  done
  cp us/nitech_us_awb_arctic_hts/hts/README.htsvoice $OLDPWD/README.htsvoice
  find . -name 'README*' -exec rm {} \;
popd

cp COPYING.nitech_us_bdl_arctic_hts COPYING.nitech_us_awb_arctic_hts
cp -a lib/voices %{buildroot}%{_datadir}/festival/lib
mkdir -p %{buildroot}%{_datadir}/festival/lib/voices/es/
cp -a festival-spanish-voices-1.0.0/* %{buildroot}%{_datadir}/festival/lib/voices/es/
cp %{SOURCE301} .

make INSTALLED_BIN=%{buildroot}%{_bindir} make_installed_bin_static
install -m 755 bin/text2wave %{buildroot}%{_bindir}
cp -a src/lib/libFestival.so* %{buildroot}%{_libdir}
install -m 755 examples/saytime %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
cp -a doc/*.1 %{buildroot}%{_mandir}/man1

pushd lib
  mkdir -p %{buildroot}%{_datadir}/festival/lib
  for f in *.scm festival.el *.ent *.gram *.dtd *.ngrambin speech.properties ; do
    install -m 644 $f %{buildroot}%{_datadir}/festival/lib/
  done
  mkdir -p %{buildroot}%{_datadir}/festival/lib/multisyn/
  install -m 644 multisyn/*.scm %{buildroot}%{_datadir}/festival/lib/multisyn/
popd

pushd lib/etc
  mkdir -p %{buildroot}%{_datadir}/festival/lib/etc
  install -m 755 email_filter %{buildroot}%{_datadir}/festival/lib/etc
  mkdir -p %{buildroot}%{_libdir}/festival/etc
  install -m 755 */audsp %{buildroot}%{_libdir}/festival/etc
popd

mkdir -p %{buildroot}%{_sysconfdir}/festival
rm %{buildroot}%{_datadir}/festival/lib/siteinit.scm
mkdir -p %{buildroot}%{_datadir}/festival/examples/
install -m 644 examples/intro.text %{buildroot}%{_datadir}/festival/examples
mkdir -p %{buildroot}%{_includedir}/festival
cp -a src/include/* %{buildroot}%{_includedir}/festival

pushd festdoc-%{docversion}/speech_tools/doc
  rm -fr CVS arch_doc/CVS man/CVS  speechtools/arch_doc/CVS
  rm -f .*_made .speechtools_html .tex_done
popd

mkdir -p %{buildroot}%{_infodir}
cp -p festdoc-%{docversion}/festival/info/* %{buildroot}%{_infodir}

%ldconfig_scriptlets

%post help
/sbin/install-info %{_infodir}/festival.info.gz %{_infodir}/dir --section "Accessibility" > /dev/null 2>&1

%preun help
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/festival.info.gz %{_infodir}/dir --section "Accessibility" > /dev/null 2>&1
fi

%files
%defattr(-,root,root)
%doc README README.*
%license COPYING COPYING.*

%dir %{_sysconfdir}/festival

%{_bindir}/*
%dir %{_datadir}/festival
%{_datadir}/festival/*

%{_libdir}/libFestival.so.*
%{_libdir}/libestbase.so.*
%{_libdir}/libestools.so.*
%{_libdir}/libeststring.so.*
%dir %{_libdir}/festival
%{_libdir}/festival/etc/*

%dir %{_libexecdir}/speech-tools
%{_libexecdir}/speech-tools/*

%files devel
%defattr(-,root,root)
%doc festdoc-%{docversion}/speech_tools
%{_libdir}/lib*.so
%dir %{_includedir}/festival
%{_includedir}/festival/*
%dir %{_includedir}/speech_tools
%{_includedir}/speech_tools/*

%files help
%defattr(-,root,root)
%doc ACKNOWLEDGMENTS NEWS
%doc festdoc-%{docversion}/festival/html/*html
%{_infodir}/*
%{_mandir}/man1/*

%changelog
* Tue Jun 06 2023 wu lei <wu_lei@hoperun.com> - 1.96-46
- Add PIE flags

* Tue Mar 15 2022 yaoxin <yaoxin30@huawei.com> - 1.96-45
- Fix CVE-2010-3996

* Tue Aug 03 2021 wangyong <wangyong187@huawei.com> - 1.96-44
- Fix build error caused by GCC upgrade to GCC-10

* Thu Nov 26 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1.96-43
- install-info should be executed before the help package is uninstalled

* Mon May 25 2020 Captain Wei <captain.a.wei@gmail.com> - 1.96-42
- add -fpermissive compile option

* Fri Apr 3 2020 chenli <chenli147@huawei.com> - 1.96-41
- modify spec

* Thu Dec 5 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.96-40
- Package init
